#![feature(test)]
extern crate test;

use nalgebra::geometry::Point3;

#[derive(Debug)]
struct Area {
    bl: Point3<u32>,
    si: u32
}

impl Area {
    fn new(pos: Point3<u32>, size: u32) -> Area {
        Area {
            bl: pos,
            si: size
        }
    }

    fn contains(&self, item: &Point3<u32>) -> bool {
        item[0] >= self.bl[0]           &&
        item[0] <  self.bl[0] + self.si &&
        item[1] >= self.bl[1]           &&
        item[1] <  self.bl[1] + self.si &&
        item[2] >= self.bl[2]           &&
        item[2] <  self.bl[2] + self.si 
    }
}

#[derive(Debug)]
pub struct OctreeNode<T> {
    neighbors: [Option<Box<OctreeNode<T>>>; 8],
    data: Option<(Point3<u32>, T)>,
    area: Area
}

impl<T> OctreeNode<T> {
    fn new(area: Area) -> OctreeNode<T> {
        OctreeNode {
            neighbors: Default::default(),
            data: None,
            area: area
        }
    }

    #[inline]
    fn create_pos(&self, index: usize) -> Area {
        let helper = |comp, b| -> u32 {
            if (comp / b) % 2 > 0 {
                1
            }else{
                0
            }
        };

        Area {
            bl: Point3::new(
                (helper(index, 1) * (self.area.si/2)) + self.area.bl[0],
                (helper(index, 2) * (self.area.si/2)) + self.area.bl[1],
                (helper(index, 4) * (self.area.si/2)) + self.area.bl[2],
            ),
            si: self.area.si/2
        }
    }

    fn which_neighbor(&self, loc: &Point3<u32>) -> usize {
        for (index, neighbor) in self.neighbors.iter().enumerate() {
            match neighbor {
                Some(ref neighbor) => {
                    if neighbor.area.contains(loc) {
                        return index;
                    }
                },
                None => {
                    if self.create_pos(index).contains(loc) {
                        return index;
                    }
                }
            }
        }
        panic!("invalid location");
    }

    pub fn insert(&mut self, item: T, loc: Point3<u32>) {
        if self.data.is_some() {
            let (loc, data) = self.data.take().unwrap();

            self.insert(data, loc);
        }

        if self.area.si == 1 {
            assert!(self.area.contains(&loc));
            self.data = Some((loc, item));

            return;
        }

        let ind = self.which_neighbor(&loc);
        match self.neighbors[ind] {
            Some(ref mut n) => {
                n.insert(item, loc);
            },
            None => {
                let area = self.create_pos(ind);    
                let mut new_node = Box::new(OctreeNode::new(area));

                new_node.data = Some((loc, item));
                //new_node.insert(item, loc);

                self.neighbors[ind] = Some(new_node);
            }
        }
    }

    pub fn get(&mut self, loc: Point3<u32>) -> Option<T> {
        match self.data.take() {
            Some((dat_loc, item)) => {
                if dat_loc == loc {
                    return Some(item);
                }else{
                    return None;
                }
            },
            None => ()
        }

        let ind = self.which_neighbor(&loc);

        match &mut self.neighbors[ind] {
            Some(node) => {
                node.get(loc)
            },
            None => None
        }
    }

}

#[derive(Debug)]
pub struct Octree<T> {
    root: OctreeNode<T>,
}

impl<T> Octree<T> {
    pub fn new(size: u32) -> Octree<T> {
        Octree {
            root: OctreeNode::new(Area::new(Point3::new(0, 0, 0), size)),
        }
    }

    pub fn insert(&mut self, item: T, loc: Point3<u32>) {
        self.root.insert(item, loc);
    }

    pub fn get(&mut self, loc: Point3<u32>) -> Option<T> {
        self.root.get(loc)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let mut oct: Octree<u32> = Octree::new(2048);
        let insert_pos = Point3::new(15, 22, 33);
        let insert_val = rand::random();

        for x in 0..64 {
            for y in 0..63 {
                for z in 0..64 {
                    let pos = Point3::new(x, y, z);
                    if pos == insert_pos {
                        continue;
                    }

                    oct.insert(x + y + z, Point3::new(x, y, z));
                }
            }
        }

        oct.insert(insert_val, insert_pos);
        let num = oct.get(insert_pos);

        assert_eq!(num.unwrap(), insert_val);
    }
}
